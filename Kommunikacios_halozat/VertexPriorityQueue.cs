﻿using System;
using System.Collections.Generic;

namespace Kommunikacios_halozat
{
    class MaxHeap<T> where T : IComparable<T>
    {
        private List<T> array = new List<T>();

        public void Add(T element)
        {
            array.Add(element);

            /*
            int c = array.Count - 1;
            while (c > 0 && array[c].CompareTo(array[c / 2]) == -1)
            {
                T tmp = array[c];
                array[c] = array[c / 2];
                array[c / 2] = tmp;
                c = c / 2;
            }
            */
        }

        public void Refresh()
        {
            array.Sort();
        }

        public T RemoveMax()
        {
            Refresh(); // elég nekünk a beépített rendezés..

            T ret = array[array.Count - 1];
            array.RemoveAt(array.Count - 1);

            /*array[0] = array[array.Count - 1];
            array.RemoveAt(array.Count - 1);

            int c = 0;
            while (c < array.Count)
            {
                int min = c;
                if (2 * c + 1 < array.Count && array[2 * c + 1].CompareTo(array[min]) == -1)
                    min = 2 * c + 1;
                if (2 * c + 2 < array.Count && array[2 * c + 2].CompareTo(array[min]) == -1)
                    min = 2 * c + 2;

                if (min == c)
                    break;
                else
                {
                    T tmp = array[c];
                    array[c] = array[min];
                    array[min] = tmp;
                    c = min;
                }
            }
            */

            return ret;
        }

        public T Peek()
        {
            return array[array.Count - 1];
        }

        public int Count
        {
            get
            {
                return array.Count;
            }
        }
    }

    /// <summary>
    /// Most pedig formáljuk a saját formánkra a Templatet
    /// </summary>
    class VertexPriorityQueue
    {
        private MaxHeap<Vertex> maxHeap;

        public VertexPriorityQueue()
        {
            maxHeap = new MaxHeap<Vertex>();
        }
        public VertexPriorityQueue(Graph g)
        {
            maxHeap = new MaxHeap<Vertex>();
            foreach (var v in g.V)
            {
                maxHeap.Add(v);
            }
        }

        public void Add(Vertex v)
        {
            maxHeap.Add(v);
        }

        public Vertex RemoveMax()
        {
            return maxHeap.RemoveMax();
        }

        public void Refresh()
        {
            maxHeap.Refresh();
        }

        public Vertex Peek()
        {
            return maxHeap.Peek();
        }

        public int Count
        {
            get
            {
                return maxHeap.Count;
            }
        }

        public bool IsEmpty { get { return Count == 0; } }
    }
}
