﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kommunikacios_halozat
{
    class Graph
    {
        public List<Edge> E { get; } // Éllista, kívülről csak lekérhető
        public List<Vertex> V { get; } // Csúcslista, kívülről csak lekérhető

        public Graph()
        {
            E = new List<Edge>();
            V = new List<Vertex>();
        }

        public void AddVertex(Vertex v) // csúcs hozzáadás, ha megvan a csúcs (ref -je)
        {
            V.Add(v);
        }

        public void AddEdge(Edge e) // Él hozzáadása, ha megvan az él (ref-je)
        {
            E.Add(e);
        }

        public Vertex GetVertex(string id) // Kérjük le a csúcs címét azonosító alapján
        {
            foreach (var v in V)
            {
                if(v.id == id.ToUpper())
                {
                    return v;
                }
            }
            return null;
        }

        public bool IsEmpty { get { return V.Count == 0; } }

        /// <summary>
        /// Hozzáad egy élet két Csúcs azonosító szerint.
        /// ELŐFELTÉTEL: Létezzen már a csúcs!
        /// </summary>
        /// <param name="Vertex1"></param>
        /// <param name="Vertex2"></param>
        public void AddEdge(string Vertex1, string Vertex2, double weight)
        {
            Vertex a = null; Vertex b = null;

            foreach (var v in V)
            {
                if(v.id == Vertex1.ToUpper())
                {
                    a = v;
                }
                if(v.id == Vertex2.ToUpper())
                {
                    b = v;
                }
            }

            if (a is null || b is null) return;
            AddEdge(new Edge(ref a, ref b, weight));
        }

        public Edge GetEdge(Vertex a, Vertex b) // két csúcs (ref) közötti élet adja vissza
        {
            foreach (var edge in E)
            {
                if(edge.A == a && edge.B == b || edge.A == b && edge.B == a)
                {
                    return edge;
                }
            }
            return null;
        }

        public List<Vertex> Neighbours(Vertex u) // a szomszédos csúcsokat gyűjti ki
        {
            List<Vertex> data = new List<Vertex>();
            foreach (var e in E)
            {
                if(e.Contains(u))
                {
                    if(!data.Contains(e.GetAnother(u)))
                    {
                        data.Add(e.GetAnother(u));
                    }
                }
            }
            return data;
        }
        
    }

    class Vertex : IComparable<Vertex>
    {
        static int counter = 0;
        public string id;
        public double Cost { get; set; }
        public bool Used { get; set; }
        public Vertex Prev { get; private set; }

        public Vertex()
        {
            ResettoDefault();
            id = counter.ToString();
            counter++;
        }
        public Vertex(string id)
        {
            ResettoDefault();
            this.id = id.ToUpper();
        }
        public void SetPreview(Vertex prev)
        {
            Prev = prev;
        }
        public void ResettoDefault()
        {
            Cost = 0;
            Used = false;
            Prev = null;
        }
        public int CompareTo(Vertex other) // Comparer függvény a rendezéshez
        {
            return Cost.CompareTo(other.Cost);
        }
    }

    class Edge
    {
        public Vertex A { get; }
        public Vertex B { get; }
        public double Weight { get; }

        public Edge(ref Vertex a, ref Vertex b, double weight)
        {
            A = a;
            B = b;
            Weight = weight;
        }
        public bool Contains(Vertex a)
        {
            return (A == a || B == a);
        }

        internal Vertex GetAnother(Vertex u)
        {
            if (Contains(u))
            {
                if (A == u) return B;
                else return A;
            }
            else return null;
        }
    }
}
