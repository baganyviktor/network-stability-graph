﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace Kommunikacios_halozat
{
    class Kommunikacios_halozat
    {
        static void Main()
        {
            Graph G = new Graph(); // Hozzuk létre a gráfunkat

            List<string> V_txt = new List<string>();
            string[] datas = new string[3];
            string source = "";
            string dest = "";

            string file = "";
            Console.Write("Graph file? (graph.txt): ");
            file = Console.ReadLine();

            try
            {
                StreamReader sr = new StreamReader(file == "" ? "graph.txt" : file);
                while (sr.Peek() > -1)
                {
                    string line = sr.ReadLine();
                    if (line == "") continue;
                    if (line.First() == '#') continue;

                    if (line.First() == '@')
                    {
                        source = line.Split(' ')[1];
                        dest = line.Split(' ')[3];
                        continue;
                    }
                    datas = line.Split(' ');
                    if (!V_txt.Contains(datas[0]))
                    {
                        G.AddVertex(new Vertex(datas[0]));
                        V_txt.Add(datas[0]);
                    }
                    if (datas.Count() < 3 || Convert.ToDouble(datas[1]) == 0)
                    {
                        Console.WriteLine("Added " + datas[0] + " without Edge!");
                    }
                    else
                    {
                        if (!V_txt.Contains(datas[2]))
                        {
                            G.AddVertex(new Vertex(datas[2]));
                            V_txt.Add(datas[2]);
                        }
                        if (Convert.ToDouble(datas[1]) < 0 || Convert.ToDouble(datas[1]) > 1)
                        {
                            Console.WriteLine("BAD INPUT DATA! BREAKING...");
                            sr.Close();
                            Console.ReadKey();
                            Environment.Exit(1);
                        }
                        G.AddEdge(datas[0], datas[2], Convert.ToDouble(datas[1]));
                        Console.WriteLine("Added " + datas[0] + " -- " + datas[2] + " with stability " + Convert.ToDouble(datas[1]));
                    }
                }
                sr.Close();

                if (G.IsEmpty)
                {
                    Console.WriteLine("Üres gráf!");
                    Console.ReadLine();
                    Environment.Exit(0);
                }
                Console.WriteLine();
                Console.WriteLine("Findig maximum stability connection from " + source.ToUpper() + " to " + dest.ToUpper());
                Console.WriteLine(); Console.WriteLine();

                // Toljuk rá az algoritmust

                Algorithm alg = new Algorithm(G, source, dest);
                alg.GetReady();
                alg.Start();
                alg.Print();
                Console.ReadKey();
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File is not exists!");
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("Something went wrong!");
                Console.ReadKey();
            }
        }
    }
}