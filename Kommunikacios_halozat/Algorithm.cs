﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kommunikacios_halozat
{
    class Algorithm
    {
        private Graph G;
        private Vertex Source;
        private Vertex Dest;
        private VertexPriorityQueue Q;

        public Algorithm(Graph g, string source, string dest)
        {
            G = g;
            Source = G.GetVertex(source.ToUpper());
            Dest = G.GetVertex(dest.ToUpper());
            Q = new VertexPriorityQueue(g);
        }

        /// <summary>
        /// A Starthoz készíti elő a gráfot, ha esetleg újra szeretnénk futtatni az Algoritmust. (vagy ha nincs megcsinálva default)
        /// </summary>
        public void GetReady()
        {
            foreach (var v in G.V)
            {
                v.ResettoDefault();
            }
            Source.Cost = 1;
        }

        /// <summary>
        /// Az algoritmus indítása
        /// </summary>
        public void Start()
        {
            while (!Q.IsEmpty)
            {
                Vertex u = Q.RemoveMax(); // Kivesszük a maximumot a prioritásos sorból

                Console.WriteLine("Out: "+u.id + " stabilty: " + u.Cost);

                /*
                 * Az u összes szomszédjára végrehajtjuk a módosított Dijkstra algoritmus "lelkét"
                */
                foreach (var v in G.Neighbours(u))
                {
                    Console.WriteLine("\tCheck: " + v.id + " stability: " + (u.Cost * G.GetEdge(u, v).Weight));
                    if (v.Cost < u.Cost * G.GetEdge(u,v).Weight)
                    {
                        v.Cost = u.Cost * G.GetEdge(u, v).Weight;
                        v.SetPreview(u);
                        Console.WriteLine("\t\t " + u.id + " <- " + v.id);
                    }
                    
                }
                
            }
        }

        /// <summary>
        /// Az eredmény kiírására ad lehetőséget. 
        /// </summary>
        public void Print()
        {
            StringBuilder sb = new StringBuilder();
            Console.WriteLine("");

            Vertex current = Dest;
            while (current != Source)
            {
                if (current.Cost == 0)
                {
                    Console.WriteLine(" # --> No connection found from "+Source.id+" to "+Dest.id);
                    //current = Source;
                    break;
                }
                else
                {
                    sb.Insert(0,("-->"+current.id));
                    current = current.Prev;
                }
            }
            if (current == Source)
            {
                sb.Insert(0, current.id);
                Console.WriteLine(sb.ToString() + " Stability summary: " + Dest.Cost);
            }
        }
    }
}
